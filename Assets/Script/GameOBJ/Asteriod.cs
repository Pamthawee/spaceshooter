using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class Asteriod : MonoBehaviour
{
    [SerializeField] private float asteroidHp;
    private float force;
    [SerializeField] private int minForce;
    [SerializeField] private int maxForce;
    [SerializeField] private GameObject item;
    [SerializeField] private GameObject powerUp;
    [SerializeField] private Transform asteroidTransform;
    
    

    void Start()
    {
        Random random = new Random();
        force = random.Next(minForce, maxForce);
        GetComponent<Rigidbody2D>().AddForce(Vector2.down * force);
        GetComponent<Rigidbody2D>().AddTorque(100);
    }

    private void FixedUpdate()
    {
        Debug.Log(force);
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            asteroidHp -= 50;
            Destroy(other.gameObject);
            
            Random random = new Random();
            int randomItem = random.Next(0, 5);
            int randomPowerUp = random.Next(0, 6);
            
            if (asteroidHp <= 0)
            {
                if (randomItem == 0)
                {
                    Instantiate(item, asteroidTransform.position, asteroidTransform.rotation);
                }

                if (randomPowerUp == 0)
                {
                    Instantiate(powerUp, asteroidTransform.position, asteroidTransform.rotation);
                }
                Destroy(gameObject);
            }
            
            
        }
    }
    
}
