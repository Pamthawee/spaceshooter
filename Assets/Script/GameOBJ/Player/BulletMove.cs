using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

public class BulletMove : MonoBehaviour
{
    [SerializeField]private float speed;
    [SerializeField] protected AudioSource audioSource;
    public float damage;
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = transform.up * speed;
        SoundManager.Instance.Play(audioSource,SoundManager.Sound.Fire);
    }
    
}
