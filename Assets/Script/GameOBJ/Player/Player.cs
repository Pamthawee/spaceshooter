using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MyNamespace
{
    public class Player : MonoBehaviour
    {
        //Movement
        [SerializeField] private float speed;

        //Boundary
        [SerializeField] private Player playerSpaceship;
        private float minX;
        private float maxX;
        private float minY;
        private float maxY;
        
        //Fire
        [SerializeField] private GameObject bullet;
        [SerializeField] private Transform bulletSpawn;
        [SerializeField] private float fireRate;
        [SerializeField] private float maxFireRate;
        private float TimeRespawnBullet;

        //Status
        public bool isDead = false;
        public EnemyBullet enemyBullet;
        [SerializeField] private float maxHp;
        [SerializeField] private float hp;
        [SerializeField] private Text hpText;

        [SerializeField] private SoundEffect sound;
        //Score
        public ScoreCounting score;
        
        void Start()
        {
            CreateMovementBoundary();
            hpText.text = $"HP  :{hp}/{maxHp}";
        }
        
        void Update()
        {
            //For move and Boundary
            Move();
            
            //For Shoot
            Fire();
        }

        void Move()
        {
            //Player Move X Y
            var move = new Vector3(Input.GetAxis("Horizontal") , Input.GetAxis("Vertical"));
            gameObject.transform.Translate(move * speed * Time.deltaTime);
            
            //Boundary move
            var newPosition = transform.position;
            newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);

            transform.position = newPosition;
        }
        
        // Create Boundary
        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, "Main camera cannot be null");
            
            var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();
            Debug.Assert(spriteRenderer != null, "spriteRenderer cannot be null");
            
            var offset = spriteRenderer.bounds.size;
            minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }
        
        //Fire
        void Fire()
        {
            if (Input.GetKey(KeyCode.Space) && Time.time > TimeRespawnBullet)
            {
                TimeRespawnBullet = Time.time + fireRate;
                Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation);
            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.tag == "Enemy" || other.gameObject.tag == "EnemyBullet" || other.gameObject.tag == "Asteroid" || other.gameObject.tag == "Boss")
            {
                if (other.gameObject.tag == "Boss")
                {
                    hp = 0;
                }
                else
                {
                    Destroy(other.gameObject);
                }
                hp -= 50;
                hpText.text = $"{hp}/{maxHp} : HP";
                if (hp <= 0)
                {
                    isDead = true;
                    hpText.text = "";
                }
                score.LostPoint();
                IsDeadEffect();
            }

            if (other.gameObject.tag == "Item")
            {
                
                if (hp +maxHp /2 < maxHp)
                {
                    hp = maxHp/2;
                }
                else
                {
                    hp = maxHp;
                }
                hpText.text = $"{hp}/{maxHp} : HP";
                Destroy(other.gameObject);
                sound.GotItem();
                
            }

            if (other.gameObject.tag == "PowerUp")
            {
                fireRate -= 0.05f;
                if (fireRate <= maxFireRate)
                {
                    fireRate = maxFireRate;
                    maxHp += 25;
                    hpText.text = $"{hp}/{maxHp} : HP";
                }
                Destroy(other.gameObject);
                sound.GotItem();
            }
        }

        private void IsDeadEffect()
        {
            if (isDead == true)
            {
                sound.PlayerDead();
                Destroy(gameObject);
            } 
        }
    }
}