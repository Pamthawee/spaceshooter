using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    [SerializeField]private float speed;
    [SerializeField] protected AudioSource audioSource;
    public float damage;
    void Start()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.down * speed;
        SoundManager.Instance.Play(audioSource,SoundManager.Sound.EnemyBullet);
    }
}
