using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private float enemySpeed;

    private float minDistanceToPlayer = 0.0f;

    [SerializeField] private float hp;
    [SerializeField] private BulletMove bulletDamage;
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform bulletSpawn;
    [SerializeField] private float fireRate;
    private float TimeRespawnBullet;

    public SoundEffect soundEffect;

    public ScoreCounting score;

    [SerializeField] private GameObject item;
    [SerializeField] private GameObject powerUp;
    [SerializeField] private Transform enemyTransform;

    private bool foundTarget;
    
    void Update()
    {
        MoveToPlayer();
        MoveToScene();
    }

    void MoveToScene()
    {
        if (!foundTarget)
        {
          transform.Translate(Vector3.down * enemySpeed * Time.deltaTime);  
        }
    }
    
    void MoveToPlayer()
    {
        tarGetCheck();

        if (foundTarget)
        {
            Vector2 displacementFromPlayer = (playerTransform.position - transform.position);
            Vector2 directionToPlayer = displacementFromPlayer.normalized;
            Vector2 enemyVelocity = directionToPlayer * enemySpeed;
            
            if (displacementFromPlayer.magnitude > minDistanceToPlayer)
            {
                transform.Translate((enemyVelocity * Time.deltaTime));
            }
            
            if (hp <= 0)
            {
                Random random = new Random();
                int randomItem = random.Next(0, 5);
                int randomPowerUp = random.Next(0, 6);
                if (randomItem == 0)
                {
                    Instantiate(item, enemyTransform.position, enemyTransform.rotation);
                }

                if (randomPowerUp == 0)
                {
                    Instantiate(powerUp, enemyTransform.position, enemyTransform.rotation);
                }
                
                soundEffect.EnemyDead();
                score.AddPoint();
                Destroy(gameObject);
            }
            Fire();
        }
    }
    

    void tarGetCheck()
    {
        GameObject[] player;
        player = GameObject.FindGameObjectsWithTag("Player");
        
        if (player.Length == 0)
        {
            foundTarget = false;
            GameObject enemyDis = GameObject.FindWithTag("Enemy");
            enemyDis.GetComponent<Enemy>().enabled = false;


        }
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet" && foundTarget)
        {
            hp -= bulletDamage.damage;
            Destroy(other.gameObject);
            
        }

        if (other.gameObject.tag == "MainCamera")
        {
            foundTarget = true;
        }
    }
    
    void Fire()
    {
        if (Time.time > TimeRespawnBullet)
        {
            TimeRespawnBullet = Time.time + fireRate;
            Instantiate(bullet, bulletSpawn.position, bulletSpawn.rotation,bulletSpawn);
        }
    }
}
