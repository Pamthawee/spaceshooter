using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearBullet : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bullet" || other.gameObject.tag == "EnemyBullet"  || other.gameObject.tag == "Asteroid")
        {
            Destroy(other.gameObject);
        }
    }
}
