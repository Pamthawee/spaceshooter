using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class  ScoreCounting : MonoBehaviour
{
    [SerializeField] private Text scoreText;
    [SerializeField] private float point;
    public float getPoint;
    public float lostPoint;

    public void Start()
    {
        scoreText.text = $"Score : {point}";
    }

    public void AddPoint()
    {
        point += getPoint;
        scoreText.text = $"Score : {point}";
    }

    public void LostPoint()
    {
        //point -= lostPoint;
        //scoreText.text = $"Score : {point}";
    }
}
