using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private Transform enemySpawn;
    [SerializeField] private float timeSpawn;
    private float timeRespawn;

    private void Update()
    {
        if (Time.time > timeRespawn)
        {
            timeRespawn = Time.time + timeSpawn;
            //Instantiate(enemy, enemySpawn.position, enemySpawn.rotation);
        }
    }
}
