using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Manager;
using MyNamespace;

public class GameCheck : MonoBehaviour
{
    //public Button menuButton;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text totalGame;
    [SerializeField] private Button restartButton;
    [SerializeField] private Button nextButton;

    private void Start()
    {
        //Text
        scoreText.text = "Score : 0";
        totalGame.text = "";
        
        //Button
        restartButton.enabled = false;
        restartButton.GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
        restartButton.GetComponentInChildren<Text>().color = Color.clear;
        
        nextButton.enabled = false;
        nextButton.GetComponentInChildren<CanvasRenderer>().SetAlpha(0);
        nextButton.GetComponentInChildren<Text>().color = Color.clear;
        
        
    }

    void FixedUpdate()
    {
        EnemyPlayerCheck();
    }

    void EnemyPlayerCheck()
    {
        GameObject[] enemy;
        enemy = GameObject.FindGameObjectsWithTag("Enemy");
        GameObject[] boss;
        boss = GameObject.FindGameObjectsWithTag("Boss");
        GameObject[] player;
        player = GameObject.FindGameObjectsWithTag("Player");
        Vector3 pos = scoreText.rectTransform.position;
        pos.x = 0;
        pos.y = 0;
        GameObject[] bullet;
        bullet = GameObject.FindGameObjectsWithTag("EnemyBullet");
        GameObject[] asteroid;
        asteroid = GameObject.FindGameObjectsWithTag("Asteroid");
        
        
        if (player.Length == 0)
        {
            totalGame.text = "Lose...";
            totalGame.color = Color.red;
                        
            //GameObject enemyDis = GameObject.FindWithTag("Enemy");
            //enemyDis.GetComponent<Enemy>().enabled = false;
        
            restartButton.enabled = true;
            restartButton.GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
            restartButton.GetComponentInChildren<Text>().color = Color.black;
            scoreText.rectTransform.position = pos;
            scoreText.fontSize = 160;
            scoreText.alignment = TextAnchor.MiddleCenter;
            SoundManager.Instance.Pause();
        }
        else if (enemy.Length == 0 && boss.Length == 0)
        {
            totalGame.text = "Win!"; 
            totalGame.color = Color.cyan;
            nextButton.enabled = true;
            nextButton.GetComponentInChildren<CanvasRenderer>().SetAlpha(1);
            nextButton.GetComponentInChildren<Text>().color = Color.black;
                
            GameObject playerDis = GameObject.FindWithTag("Player");
             
            //playerDis.GetComponent<Player>().enabled = false;
             
            scoreText.rectTransform.position = pos;
             
            scoreText.fontSize = 160;
             
            scoreText.alignment = TextAnchor.MiddleCenter;
             
            SoundManager.Instance.Pause();


            foreach (var asteroids in asteroid)
            { 
                Destroy(asteroids);
            }

            foreach (var bullets in bullet)
            {
                Destroy(bullets);
            }
        }

        
        
        
    }
}
