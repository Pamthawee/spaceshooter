using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
   public class SoundManager : MonoBehaviour
   {
       [SerializeField] private SoundClip[] soundClips;
       [SerializeField] private AudioSource audioSource;
       
       public static SoundManager Instance { get; private set; }
       
       [Serializable] public struct SoundClip
       {
           public Sound Sound;
           public AudioClip AudioClip;
       }
       
       public enum Sound
       {
           BGM,
           Fire,
           PlayerDead,
           EnemyDead,
           EnemyBullet,
           GotItem,
           BossBgm,
       }
       
       /// <summary>
       /// Play Sound
       /// </summary>
       /// <param name="audioSource"></param>
       /// <param name="sound"></param>
       public void Play(AudioSource audioSource , Sound sound)
       {
           Debug.Assert(audioSource != null , "AudioSource cannot be null");

           audioSource.clip = GetAudioClip(sound);
           audioSource.Play();
       }

       /// <summary>
       /// Play BackGroundMusic
       /// </summary>
       public void PlayBGM()
       {
           audioSource.loop = true;
           Play(audioSource,Sound.BGM);
       }
       public void PlayBossBGM()
       {
           audioSource.loop = true;
           Play(audioSource,Sound.BossBgm);
       }
       private AudioClip GetAudioClip(Sound sound)
       {
           foreach (var soundClip in soundClips)
           {
               if (soundClip.Sound == sound)
               {
                   return soundClip.AudioClip;
               }
           }
           
           Debug.Assert(false,$"Cannot find sound {sound}");
           return null;
       }
       
       
       private void Awake()
       {
           Debug.Assert(audioSource != null, "audioSource cannot be null");
           Debug.Assert(soundClips != null && soundClips.Length != 0,"");

           if (Instance == null)
           {
               Instance = this;
           }
           
           //DontDestroyOnLoad(this);
       }

       public void Pause()
       {
           audioSource.Pause();
       }
       public void Start()
       {
           Instance.PlayBGM();
       }
       
   } 
}
