using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;

public class SoundEffect : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    // Start is called before the first frame update
    
    
    public void PlayerDead()
    {
        audioSource.loop = false;
        SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerDead);
    }

    public void EnemyDead()
    {
        audioSource.loop = false;
        SoundManager.Instance.Play(audioSource,SoundManager.Sound.EnemyDead);
    }
    
    public void Asteroid()
    {
        audioSource.loop = false;
        SoundManager.Instance.Play(audioSource,SoundManager.Sound.EnemyBullet);
    }

    public void GotItem()
    {
        audioSource.loop = false;
        SoundManager.Instance.Play(audioSource,SoundManager.Sound.GotItem);
    }
}
