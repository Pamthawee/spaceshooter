using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonControl : MonoBehaviour
{

    public void ToMainGame()
    {
        SceneManager.LoadScene("MainGame");
    }

    public void RestartLevelOne()
    {
        SceneManager.LoadScene("MainGame");
    }

    public void NextLevel2()
    {
        SceneManager.LoadScene("MainGameLV2");
    }
    
    public void RestartLevelTwo()
    {
        SceneManager.LoadScene("MainGameLV2");
    }

    public void WiningScene()
    {
        SceneManager.LoadScene("WiningScene");
    }

    public void ToMenuGame()
    {
        SceneManager.LoadScene("MenuGame");
    }

    public void Exit()
    {
        Application.Quit();
    }
}
