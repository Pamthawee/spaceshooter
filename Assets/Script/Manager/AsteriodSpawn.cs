using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AsteriodSpawn : MonoBehaviour
{
    [SerializeField] private GameObject asteroid;
    [SerializeField] private float fireRateRespawn;
    private float TimeRespawn;
    

    void Update()
    {
        SpawnWaves();
        GameObject[] player;
        player = GameObject.FindGameObjectsWithTag("Player");
        if (player.Length == 0)
        {
            Destroy(gameObject);
        }
        
    }
    
    void SpawnWaves()
    {
        if (Time.time > TimeRespawn)
        {
            Vector3 spawnPosition = new Vector3(Random.Range(-8,8), 6f, 0.0f);
            Quaternion spawnRotation = Quaternion.identity;
            Instantiate(asteroid, spawnPosition, spawnRotation);
            TimeRespawn = Time.time + fireRateRespawn;
        }
    }
    
}
