using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemMove : MonoBehaviour
{
    [SerializeField] private float force;
    void Start()
    {
        GetComponent<Rigidbody2D>().AddForce(Vector2.down * force);
    }

    
}
