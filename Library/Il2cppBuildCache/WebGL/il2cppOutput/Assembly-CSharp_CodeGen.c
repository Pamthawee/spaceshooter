﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void BossMove::Update()
extern void BossMove_Update_m989AE6CB1F355B2A75190B76BD968E3CF0980664 (void);
// 0x00000002 System.Void BossMove::MoveToScene()
extern void BossMove_MoveToScene_mADF9270BD709F4B9736D3022F27CB67A78DBBA9C (void);
// 0x00000003 System.Void BossMove::MoveToPlayer()
extern void BossMove_MoveToPlayer_mB48C1D060B1445FF5857D95593299F498E3CD833 (void);
// 0x00000004 System.Void BossMove::tarGetCheck()
extern void BossMove_tarGetCheck_mA017F7FB8E484E069D7605100AA1199CDAEFC516 (void);
// 0x00000005 System.Void BossMove::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void BossMove_OnTriggerEnter2D_mE22BA961441A24E1AF97665F64915AF9B943C688 (void);
// 0x00000006 System.Void BossMove::Fire()
extern void BossMove_Fire_m7E320D9B827C8E0BA7ECFC6757F2D1BD3C6123D5 (void);
// 0x00000007 System.Void BossMove::.ctor()
extern void BossMove__ctor_mE699DB884208F03AFD2437CD682F3E472B12D82F (void);
// 0x00000008 System.Void ItemMove::Start()
extern void ItemMove_Start_m255B2E7C9C6637699A334A56C3D176B06F9A9912 (void);
// 0x00000009 System.Void ItemMove::.ctor()
extern void ItemMove__ctor_m9BD936ABA52D50799DC880674C1CBE858B0783B2 (void);
// 0x0000000A System.Void Asteriod::Start()
extern void Asteriod_Start_m3A223E49CBFE28A0352B52A25E89D1636A4E1327 (void);
// 0x0000000B System.Void Asteriod::FixedUpdate()
extern void Asteriod_FixedUpdate_mB349BB52E21E9F2227EF91A2DC2C5CD3454DFDE9 (void);
// 0x0000000C System.Void Asteriod::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Asteriod_OnTriggerEnter2D_mA782A33BD820EA9CF1839D08FBF7176F1C4474F7 (void);
// 0x0000000D System.Void Asteriod::.ctor()
extern void Asteriod__ctor_m0B2242791BA862F4F150386C12789764185E01BF (void);
// 0x0000000E System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x0000000F System.Void Enemy::MoveToScene()
extern void Enemy_MoveToScene_m6963211515C0B1845BD66FA817E9A6FFA38A429F (void);
// 0x00000010 System.Void Enemy::MoveToPlayer()
extern void Enemy_MoveToPlayer_m217EE8299FCE8B75A067088586D2539292E9132D (void);
// 0x00000011 System.Void Enemy::tarGetCheck()
extern void Enemy_tarGetCheck_mC831C3A44E378C4EC1D8BDEAF9CCE8E8DC84B364 (void);
// 0x00000012 System.Void Enemy::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Enemy_OnTriggerEnter2D_mDECB4702894034560FBA7606CDC45A5B12EB0B38 (void);
// 0x00000013 System.Void Enemy::Fire()
extern void Enemy_Fire_m3F1F6B559F5C697843C9FDF06E1A7E62691FCF8E (void);
// 0x00000014 System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x00000015 System.Void EnemyBullet::Start()
extern void EnemyBullet_Start_m63FE8026E379396D67169F6CB7CE127E56402105 (void);
// 0x00000016 System.Void EnemyBullet::.ctor()
extern void EnemyBullet__ctor_m04AD7D8C44F99D4158DDEBAA06C29912B54F9624 (void);
// 0x00000017 System.Void BulletMove::Start()
extern void BulletMove_Start_mB4DDCF8F8F2913F3AB72FFEFE1D98FA7A192F0FC (void);
// 0x00000018 System.Void BulletMove::.ctor()
extern void BulletMove__ctor_mECCC83D0FD7FF3EEA33F0F6C59171F7274E3664C (void);
// 0x00000019 System.Void AsteriodSpawn::Update()
extern void AsteriodSpawn_Update_m882C33065C880DCAAF78E3B8A4B65E324E0AF03A (void);
// 0x0000001A System.Void AsteriodSpawn::SpawnWaves()
extern void AsteriodSpawn_SpawnWaves_m308C05A80A3BE970A5E8E3E16E9CDF03A74EE6C5 (void);
// 0x0000001B System.Void AsteriodSpawn::.ctor()
extern void AsteriodSpawn__ctor_mC0F7E9C15D2320980EBCD6B945E8A8C7FFAEF655 (void);
// 0x0000001C System.Void ButtonControl::ToMainGame()
extern void ButtonControl_ToMainGame_mC0FF3951FDB71CCB176E75C30CA001C55057AE8B (void);
// 0x0000001D System.Void ButtonControl::RestartLevelOne()
extern void ButtonControl_RestartLevelOne_m599953C9700CE3490B659B60B07C5DCE4C390019 (void);
// 0x0000001E System.Void ButtonControl::NextLevel2()
extern void ButtonControl_NextLevel2_m92A384FED4A25C223DF8DEE2B978B9EF66DD3A30 (void);
// 0x0000001F System.Void ButtonControl::RestartLevelTwo()
extern void ButtonControl_RestartLevelTwo_mC3134237E2EC6456D716B47294B6829912F2AB2A (void);
// 0x00000020 System.Void ButtonControl::WiningScene()
extern void ButtonControl_WiningScene_m5405B2A4A23DFFA7A4CA3BA0121226F89A7E47C1 (void);
// 0x00000021 System.Void ButtonControl::ToMenuGame()
extern void ButtonControl_ToMenuGame_m5B821125A58928383AF24EB7271298E5E7C90802 (void);
// 0x00000022 System.Void ButtonControl::Exit()
extern void ButtonControl_Exit_m8AE024B70EC66D3DB01384F4A5E77C4DE02BDBA5 (void);
// 0x00000023 System.Void ButtonControl::.ctor()
extern void ButtonControl__ctor_m1AA2879EB9BB58535C7D8B7AC3E061801394480C (void);
// 0x00000024 System.Void ClearBullet::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ClearBullet_OnTriggerExit2D_m1868D1EF410EDDF2DCE350FF420BBCA7F970A190 (void);
// 0x00000025 System.Void ClearBullet::.ctor()
extern void ClearBullet__ctor_mB44A1ADCA3BEA2DA0E94F3A64CD907FE50800C03 (void);
// 0x00000026 System.Void EnemySpawn::Update()
extern void EnemySpawn_Update_mB9B99407B6D92276F7498D8A56515AF260780505 (void);
// 0x00000027 System.Void EnemySpawn::.ctor()
extern void EnemySpawn__ctor_mF60950C44BDFFF5FCFEFB481F9DA251DA0C1CB4A (void);
// 0x00000028 System.Void GameCheck::Start()
extern void GameCheck_Start_mCC3666C8D98EEF6BFE1FC6DDC085BD89E7AC18C9 (void);
// 0x00000029 System.Void GameCheck::FixedUpdate()
extern void GameCheck_FixedUpdate_m36D239544F97AB3C22386B7D02BA79E8D9C2C77D (void);
// 0x0000002A System.Void GameCheck::EnemyPlayerCheck()
extern void GameCheck_EnemyPlayerCheck_m6F4ED738CF4EDFC55D09C372DAF675B5A6EA36CB (void);
// 0x0000002B System.Void GameCheck::.ctor()
extern void GameCheck__ctor_m10824E4E25890703AD5DBDE8D7976E266E57C322 (void);
// 0x0000002C System.Void ScoreCounting::Start()
extern void ScoreCounting_Start_mD5CED1AE44A0D2D3FF1F39EE54F246CC2718E5B8 (void);
// 0x0000002D System.Void ScoreCounting::AddPoint()
extern void ScoreCounting_AddPoint_m2B457FCC6FE5DCEFB555B85D4518FD02E4EFFB64 (void);
// 0x0000002E System.Void ScoreCounting::LostPoint()
extern void ScoreCounting_LostPoint_m2F22DF1C024905296417BA54E274CCBC8DE9E066 (void);
// 0x0000002F System.Void ScoreCounting::.ctor()
extern void ScoreCounting__ctor_mF0F8EEB99E101DB3C22D147E9E1021955D7B6472 (void);
// 0x00000030 System.Void SoundEffect::PlayerDead()
extern void SoundEffect_PlayerDead_mD756849B3C0BC9780C66D8759740DF87F8C91E1E (void);
// 0x00000031 System.Void SoundEffect::EnemyDead()
extern void SoundEffect_EnemyDead_m65E1AD85B49415360095A0663CFDE28F5D6EE42B (void);
// 0x00000032 System.Void SoundEffect::Asteroid()
extern void SoundEffect_Asteroid_mC6C41FF86A407296FFE3B77827EC4E4E1F1CE60F (void);
// 0x00000033 System.Void SoundEffect::GotItem()
extern void SoundEffect_GotItem_m045D13682D2705F382D74AB7C1E67566815C14E8 (void);
// 0x00000034 System.Void SoundEffect::.ctor()
extern void SoundEffect__ctor_m50DE6CF211200E7FA59655EF945DAF6691933CB9 (void);
// 0x00000035 Manager.SoundManager Manager.SoundManager::get_Instance()
extern void SoundManager_get_Instance_m80B9DF1A82A9E306B511931C72D2B651932EB4DF (void);
// 0x00000036 System.Void Manager.SoundManager::set_Instance(Manager.SoundManager)
extern void SoundManager_set_Instance_m8D431EEDEB4AF2F4E49DA88693594C2C0001D0E7 (void);
// 0x00000037 System.Void Manager.SoundManager::Play(UnityEngine.AudioSource,Manager.SoundManager/Sound)
extern void SoundManager_Play_mCE2CB99074B67992C1CB0DC562603B48A3D4459B (void);
// 0x00000038 System.Void Manager.SoundManager::PlayBGM()
extern void SoundManager_PlayBGM_mDD9FEF1D0D605912278835E4213127B20F61D1B1 (void);
// 0x00000039 System.Void Manager.SoundManager::PlayBossBGM()
extern void SoundManager_PlayBossBGM_mEBD9D78DF814495B15CEF04E671F8A5657BB02BD (void);
// 0x0000003A UnityEngine.AudioClip Manager.SoundManager::GetAudioClip(Manager.SoundManager/Sound)
extern void SoundManager_GetAudioClip_m8DD7A794A6C3400B9F9B911F3A02AA09B755BB09 (void);
// 0x0000003B System.Void Manager.SoundManager::Awake()
extern void SoundManager_Awake_m58A6526C76DA16AB6E47AD0A47F35E9ABD489945 (void);
// 0x0000003C System.Void Manager.SoundManager::Pause()
extern void SoundManager_Pause_m5B50D0445A549959FE0BA6E133086A9A68A69019 (void);
// 0x0000003D System.Void Manager.SoundManager::Start()
extern void SoundManager_Start_m658475EF80AC71899DDD38F2FF197C42BAA9386E (void);
// 0x0000003E System.Void Manager.SoundManager::.ctor()
extern void SoundManager__ctor_m82D5AF6FC16270F11B1EE8A880814DA38E044698 (void);
// 0x0000003F System.Void MyNamespace.Player::Start()
extern void Player_Start_m28C4F0CCDF6747E229A258D1248AC34A72F83F1D (void);
// 0x00000040 System.Void MyNamespace.Player::Update()
extern void Player_Update_mCA1D841AA0C3FF11938F97D6482444AAEC64FC73 (void);
// 0x00000041 System.Void MyNamespace.Player::Move()
extern void Player_Move_m1853B1921BCD75D4519DE8963BF9357F8220481D (void);
// 0x00000042 System.Void MyNamespace.Player::CreateMovementBoundary()
extern void Player_CreateMovementBoundary_mE32AD22BAC59878F23B3307AC930875B57CFB3D4 (void);
// 0x00000043 System.Void MyNamespace.Player::Fire()
extern void Player_Fire_m68E9E781756E0F776C513717DE5E1AD3E273421D (void);
// 0x00000044 System.Void MyNamespace.Player::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Player_OnTriggerEnter2D_m9703917F9FF315A4667C2951ACC91F587EBA6798 (void);
// 0x00000045 System.Void MyNamespace.Player::IsDeadEffect()
extern void Player_IsDeadEffect_m5DDF88731EE02F50DE585BB0551FD8C0448EB97F (void);
// 0x00000046 System.Void MyNamespace.Player::.ctor()
extern void Player__ctor_m82F9EC01F8FC1C47B54DFDDCBCA50FC8F0EE9DF6 (void);
static Il2CppMethodPointer s_methodPointers[70] = 
{
	BossMove_Update_m989AE6CB1F355B2A75190B76BD968E3CF0980664,
	BossMove_MoveToScene_mADF9270BD709F4B9736D3022F27CB67A78DBBA9C,
	BossMove_MoveToPlayer_mB48C1D060B1445FF5857D95593299F498E3CD833,
	BossMove_tarGetCheck_mA017F7FB8E484E069D7605100AA1199CDAEFC516,
	BossMove_OnTriggerEnter2D_mE22BA961441A24E1AF97665F64915AF9B943C688,
	BossMove_Fire_m7E320D9B827C8E0BA7ECFC6757F2D1BD3C6123D5,
	BossMove__ctor_mE699DB884208F03AFD2437CD682F3E472B12D82F,
	ItemMove_Start_m255B2E7C9C6637699A334A56C3D176B06F9A9912,
	ItemMove__ctor_m9BD936ABA52D50799DC880674C1CBE858B0783B2,
	Asteriod_Start_m3A223E49CBFE28A0352B52A25E89D1636A4E1327,
	Asteriod_FixedUpdate_mB349BB52E21E9F2227EF91A2DC2C5CD3454DFDE9,
	Asteriod_OnTriggerEnter2D_mA782A33BD820EA9CF1839D08FBF7176F1C4474F7,
	Asteriod__ctor_m0B2242791BA862F4F150386C12789764185E01BF,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_MoveToScene_m6963211515C0B1845BD66FA817E9A6FFA38A429F,
	Enemy_MoveToPlayer_m217EE8299FCE8B75A067088586D2539292E9132D,
	Enemy_tarGetCheck_mC831C3A44E378C4EC1D8BDEAF9CCE8E8DC84B364,
	Enemy_OnTriggerEnter2D_mDECB4702894034560FBA7606CDC45A5B12EB0B38,
	Enemy_Fire_m3F1F6B559F5C697843C9FDF06E1A7E62691FCF8E,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	EnemyBullet_Start_m63FE8026E379396D67169F6CB7CE127E56402105,
	EnemyBullet__ctor_m04AD7D8C44F99D4158DDEBAA06C29912B54F9624,
	BulletMove_Start_mB4DDCF8F8F2913F3AB72FFEFE1D98FA7A192F0FC,
	BulletMove__ctor_mECCC83D0FD7FF3EEA33F0F6C59171F7274E3664C,
	AsteriodSpawn_Update_m882C33065C880DCAAF78E3B8A4B65E324E0AF03A,
	AsteriodSpawn_SpawnWaves_m308C05A80A3BE970A5E8E3E16E9CDF03A74EE6C5,
	AsteriodSpawn__ctor_mC0F7E9C15D2320980EBCD6B945E8A8C7FFAEF655,
	ButtonControl_ToMainGame_mC0FF3951FDB71CCB176E75C30CA001C55057AE8B,
	ButtonControl_RestartLevelOne_m599953C9700CE3490B659B60B07C5DCE4C390019,
	ButtonControl_NextLevel2_m92A384FED4A25C223DF8DEE2B978B9EF66DD3A30,
	ButtonControl_RestartLevelTwo_mC3134237E2EC6456D716B47294B6829912F2AB2A,
	ButtonControl_WiningScene_m5405B2A4A23DFFA7A4CA3BA0121226F89A7E47C1,
	ButtonControl_ToMenuGame_m5B821125A58928383AF24EB7271298E5E7C90802,
	ButtonControl_Exit_m8AE024B70EC66D3DB01384F4A5E77C4DE02BDBA5,
	ButtonControl__ctor_m1AA2879EB9BB58535C7D8B7AC3E061801394480C,
	ClearBullet_OnTriggerExit2D_m1868D1EF410EDDF2DCE350FF420BBCA7F970A190,
	ClearBullet__ctor_mB44A1ADCA3BEA2DA0E94F3A64CD907FE50800C03,
	EnemySpawn_Update_mB9B99407B6D92276F7498D8A56515AF260780505,
	EnemySpawn__ctor_mF60950C44BDFFF5FCFEFB481F9DA251DA0C1CB4A,
	GameCheck_Start_mCC3666C8D98EEF6BFE1FC6DDC085BD89E7AC18C9,
	GameCheck_FixedUpdate_m36D239544F97AB3C22386B7D02BA79E8D9C2C77D,
	GameCheck_EnemyPlayerCheck_m6F4ED738CF4EDFC55D09C372DAF675B5A6EA36CB,
	GameCheck__ctor_m10824E4E25890703AD5DBDE8D7976E266E57C322,
	ScoreCounting_Start_mD5CED1AE44A0D2D3FF1F39EE54F246CC2718E5B8,
	ScoreCounting_AddPoint_m2B457FCC6FE5DCEFB555B85D4518FD02E4EFFB64,
	ScoreCounting_LostPoint_m2F22DF1C024905296417BA54E274CCBC8DE9E066,
	ScoreCounting__ctor_mF0F8EEB99E101DB3C22D147E9E1021955D7B6472,
	SoundEffect_PlayerDead_mD756849B3C0BC9780C66D8759740DF87F8C91E1E,
	SoundEffect_EnemyDead_m65E1AD85B49415360095A0663CFDE28F5D6EE42B,
	SoundEffect_Asteroid_mC6C41FF86A407296FFE3B77827EC4E4E1F1CE60F,
	SoundEffect_GotItem_m045D13682D2705F382D74AB7C1E67566815C14E8,
	SoundEffect__ctor_m50DE6CF211200E7FA59655EF945DAF6691933CB9,
	SoundManager_get_Instance_m80B9DF1A82A9E306B511931C72D2B651932EB4DF,
	SoundManager_set_Instance_m8D431EEDEB4AF2F4E49DA88693594C2C0001D0E7,
	SoundManager_Play_mCE2CB99074B67992C1CB0DC562603B48A3D4459B,
	SoundManager_PlayBGM_mDD9FEF1D0D605912278835E4213127B20F61D1B1,
	SoundManager_PlayBossBGM_mEBD9D78DF814495B15CEF04E671F8A5657BB02BD,
	SoundManager_GetAudioClip_m8DD7A794A6C3400B9F9B911F3A02AA09B755BB09,
	SoundManager_Awake_m58A6526C76DA16AB6E47AD0A47F35E9ABD489945,
	SoundManager_Pause_m5B50D0445A549959FE0BA6E133086A9A68A69019,
	SoundManager_Start_m658475EF80AC71899DDD38F2FF197C42BAA9386E,
	SoundManager__ctor_m82D5AF6FC16270F11B1EE8A880814DA38E044698,
	Player_Start_m28C4F0CCDF6747E229A258D1248AC34A72F83F1D,
	Player_Update_mCA1D841AA0C3FF11938F97D6482444AAEC64FC73,
	Player_Move_m1853B1921BCD75D4519DE8963BF9357F8220481D,
	Player_CreateMovementBoundary_mE32AD22BAC59878F23B3307AC930875B57CFB3D4,
	Player_Fire_m68E9E781756E0F776C513717DE5E1AD3E273421D,
	Player_OnTriggerEnter2D_m9703917F9FF315A4667C2951ACC91F587EBA6798,
	Player_IsDeadEffect_m5DDF88731EE02F50DE585BB0551FD8C0448EB97F,
	Player__ctor_m82F9EC01F8FC1C47B54DFDDCBCA50FC8F0EE9DF6,
};
static const int32_t s_InvokerIndices[70] = 
{
	1985,
	1985,
	1985,
	1985,
	1611,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1611,
	1985,
	1985,
	1985,
	1985,
	1985,
	1611,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1611,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	3235,
	3166,
	958,
	1985,
	1985,
	1246,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1985,
	1611,
	1985,
	1985,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	70,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
